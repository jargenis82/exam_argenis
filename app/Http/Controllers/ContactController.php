<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactList;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactWelcomeMail;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $contacts = ContactList::all();
        $json = "";
        foreach ($contacts as $i => $contact) {
            $url_destroy = route('destroy', ['id' => $contact->id]);
            $url_edit = route('edit', ['id' => $contact->id]);
            $coma = $i == 0 ? "" : ",";
            $json .= "$coma
            [
                \"" . $contact->name . "\",
                \"" . $contact->last_name . "\",
                \"" . $contact->email . "\",
                \"" . $contact->phone_number . "\",
                \"<button type='button' class='btn btn-block btn-primary' onclick='window.location = \\\"$url_edit\\\"'></button>\",
                \"<button type='button' class='btn btn-block btn-danger btn-delete' data-url='".$url_destroy."'></button>\"
            ]
            ";
        }



        return "{
            \"data\": [
                $json
            ]
          }";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $url_store_edit = route('store');
        $create_edit = "Create";
        return view('create_edit', compact('url_store_edit', 'create_edit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $attributes = $request->all();
        $contact = new ContactList;
        $contact->name = $request->name;
        $contact->last_name = $request->last_name;
        $contact->phone_number = $request->phone_number;
        $contact->email = $request->email;
        Mail::to($request->email)->send(new ContactWelcomeMail());
        $contact->save();
        return "";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        ContactList::destroy($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $create_edit = "Edit";
        $url_store_edit = route('update', $id);
        $contact = ContactList::find($id);
        return view('create_edit', compact('url_store_edit', 'create_edit', 'contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact = ContactList::find($id);
        $contact->name = $request->name;
        $contact->last_name = $request->last_name;
        $contact->phone_number = $request->phone_number;
        $contact->email = $request->email;
        $contact->save();
        return "";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        ContactList::destroy($id);
        return redirect('home');
    }
}
