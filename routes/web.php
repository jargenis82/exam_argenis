<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/index', 'ContactController@index')->name('index');
Route::get('/create', 'ContactController@create')->name('create');
Route::get('/edit/{id}', 'ContactController@edit')->name('edit');
Route::post('/store', 'ContactController@store')->name('store');
Route::post('/update/{id}', 'ContactController@update')->name('update');
Route::get('/destroy/{id}', 'ContactController@destroy')->name('destroy');

Route::get('change-password', 'ChangePasswordController@index');
Route::post('change-password', 'ChangePasswordController@store')->name('change.password');

Route::get('update-user', 'UpdateUserController@index');
Route::post('update-user', 'UpdateUserController@store')->name('update.user');