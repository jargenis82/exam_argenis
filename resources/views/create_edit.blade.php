@extends('layouts.app')

@section('javascript')
<script>
    $(document).ready(function() {
        $("#button_cancel").on('click', function() {
            window.location = "{{route('home')}}";
        });

        $("#button_submit").on('click', function() {

            if ($("#form_contact").valid()) {
                var inputs = $("#form_contact").serialize();

                $.ajax({
                    type: 'POST',
                    url: '{{$url_store_edit}}',
                    data: inputs,
                    success: function() {
                        window.location = "{{route('home')}}";
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        alert("error");
                    }
                });
            }

        });
    });
</script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- form start -->
            <h1>{{$create_edit}} Contact</h1>
            <form role="form" id="form_contact" name="form_contact">
                @csrf
                <div class="box-body">
                    <div class="form-group">
                        <label for="name">First Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="" value="{{$contact->name ?? '' }}" required>
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="" value="{{$contact->last_name ?? '' }}" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="" value="{{$contact->email ?? '' }}" required>
                    </div>
                    <div class="form-group">
                        <label for="phone_number">Contact Number</label>
                        <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="" value="{{$contact->name ?? '' }}" required>
                    </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="button" class="btn btn-primary" id="button_submit">Submit</button>
                    <button type="button" class="btn btn-primary" id="button_cancel">Cancel</button>
                </div>
            </form>
        </div>
        <!-- /.box -->
    </div>

</div>
</div>
@endsection