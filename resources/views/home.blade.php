@extends('layouts.app')

@section('javascript')
<script>
    $(document).ready(function() {
        var url_delete = "";

        $('#example').DataTable({
            "ajax": "{{$url_index}}",
            "autoWidth": false,
            "initComplete": function(settings, json) {
                $(".btn-delete").click(function() {
                    $('#myModal').modal('show');
                    url_delete = $(this).data("url");
                });
            }
        });

        $(".btn-delete-contact").click(function() {
            window.location = url_delete;
        });


    });
</script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
        </div>
        <div class="col-md-2">
            <button type="button" class="btn btn-block btn-primary" onclick="window.location = 'create'">Create Contact</button>
            <br>
        </div>
        <div class="col-md-5">
        </div>

    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- <div class="card"> -->
            <!-- <div class="card-header">{{ __('Dashboard') }}</div> -->
            <!-- <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div> -->
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Contact Number</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Contact Number</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </tfoot>
            </table>
            <!-- </div> -->
        </div>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete Contact</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete the contact?</p>
            </div>
            <div class="modal-footer">
                <button type='button' class='btn btn-block btn-danger btn-delete-contact'>Delete</button>
                <button type='button' class='btn btn-block btn-primary' data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection